import { combineReducers } from 'redux';
import auth from './auth';
import specie from './specie'

export default combineReducers({
    auth,
    specie
});