import {
    CREATE_SPECIE_FAIL,
    CREATE_SPECIE_SUCCESS,
    SPECIE_LOADED_SUCCESS,
    SPECIE_LOADED_FAIL,
} from '../actions/types'

const initialState = {
    specie: []
};

export default function(state = initialState, action) {
    const { type, payload } = action;

    switch(type) {
        case SPECIE_LOADED_SUCCESS:
            return {
                ...state,
                specie: payload.results
            }
        case SPECIE_LOADED_FAIL:
            return {
                ...state,
                specie: []
            }
        case CREATE_SPECIE_SUCCESS:
            return {
                ...state,
                specie: []
            }
        case CREATE_SPECIE_FAIL:
            return {
                ...state,
                specie: []
            }
        default:
            return state
    }
};