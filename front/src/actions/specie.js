import axios from "axios";
import {
    CREATE_SPECIE_FAIL,
    CREATE_SPECIE_SUCCESS,
    SPECIE_LOADED_SUCCESS,
    SPECIE_LOADED_FAIL,
} from './types'


export const load_species = () => async dispatch => {
    try {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json'
            }
        }; 

        const res = await axios.get(`http://127.0.0.1:8000/species`, config);
        dispatch({
            type: SPECIE_LOADED_SUCCESS,
            payload: res.data
        });
        } catch (err) {
            dispatch({
                type: SPECIE_LOADED_FAIL
        });
    }
};


export const createSpecie = (name) => async dispatch => {
    try {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json'
            }
        };
    
        const body = JSON.stringify({ name });

        const res = axios.post('http://127.0.0.1:8000/species', body, config)

        dispatch({
            type: CREATE_SPECIE_SUCCESS,
            payload: res.data
        })
        dispatch(load_species())
    } catch (err) {
        dispatch({
            type: CREATE_SPECIE_FAIL
        })
    }
}