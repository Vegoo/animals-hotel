import React, { useState } from "react";
import { createSpecie } from '../actions/specie'
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';


const AddSpecie = ({ createSpecie }) => {
  const [ok, setok] = useState(false)
  const [Specie, setSpecie] = useState({
    name: "",
  })

  const change = (e) => {
    setSpecie({...Specie, [e.target.name]: e.target.value})
  }

  const submit = async (e) => {
    e.preventDefault();
    createSpecie(Specie.name)
    setok(true)
  }

  if (ok) {
    return <Redirect to='/specie'/>
  }

  return (
    <div className='container'>
        <form>
            <div className="form-group">
                <label >Add</label>
                <input type="text" className="form-control" name="name" placeholder="name" onChange={change}></input>
            </div>
            <button type="submit" className="btn btn-primary" onClick={submit}>Register</button>
        </form>
    </div>
  );
};

export default connect(null, { createSpecie })(AddSpecie);
