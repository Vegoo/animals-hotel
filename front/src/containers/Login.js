import axios from 'axios';
import React, { useState } from 'react'
//import { useHistory } from "react-router-dom";
import { Link, Redirect } from "react-router-dom";
import { connect } from 'react-redux'
import { login } from '../actions/auth'
 

const Login = ({ login, isAuthenticated }) => {
    const [user, setUser] = useState({
      email: "",  
      password: ""
    })

  const change = (e) => {
    setUser({...user, [e.target.name]: e.target.value})
  }

  const submit = async (e) => {
    e.preventDefault();
    login(user.email, user.password)
  }

  if(isAuthenticated) {
    return <Redirect to='/' />
  }

  return (
    <div className='container'>
      <form>
        <div className="form-group">
          <label >Email address</label>
          <input type="email" className="form-control" name="email"  aria-describedby="emailHelp" placeholder="Enter email" onChange={change}></input>
        </div>
        <div className="form-group">
          <label >Password</label>
          <input type="text" className="form-control" name="password" placeholder="Password" onChange={change}></input>
        </div>
        <button type="submit" className="btn btn-primary" onClick={submit}>Login</button>
      </form>
      </div>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps, { login })(Login);
