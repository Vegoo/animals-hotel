import axios from 'axios';
import React, { useState } from 'react'
import { connect } from 'react-redux';
import { Redirect } from "react-router-dom";
import { signup } from '../actions/auth';

const Signup = ({ signup, isAuthenticated }) => {
    const [account, setaccount] = useState(false)
    const [user, setUser] = useState({
        name: "",
        email: "",  
        surname: "",
        password: ""
    })

    const change = (e) => {
        setUser({...user, [e.target.name]: e.target.value})
    }

    const submit = async (e) => {
        e.preventDefault();
        signup(user.name, user.email, user.surname, user.password)
        setaccount(true)
    }

    if(isAuthenticated) {
        return <Redirect to='/' />
    }

    if(account) {
        return <Redirect to='/login' />
    }

    return (
        <div className='container'>
            <form>
                <div className="form-group">
                    <label >Email address</label>
                    <input type="email" className="form-control" name="email"  aria-describedby="emailHelp" placeholder="Enter email" onChange={change}></input>
                </div>
                <div className="form-group">
                    <label >Name</label>
                    <input type="text" className="form-control" name="name" placeholder="Name" onChange={change}></input>
                </div>
                <div className="form-group">
                    <label >Surname</label>
                    <input type="text" className="form-control" name="surname" placeholder="Surname" onChange={change}></input>
                </div>
                <div className="form-group">
                    <label >Password</label>
                    <input type="password" className="form-control" name="password" placeholder="Password" onChange={change}></input>
                </div>
                <button type="submit" className="btn btn-primary" onClick={submit}>Register</button>
            </form>
        </div>
    );
};

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated
  })
  
  export default connect(mapStateToProps, { signup })(Signup);
