import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios"
import { connect } from 'react-redux';
import { load_species } from '../actions/specie'

const Specie = ({ load_species, specie }) => {

  const deleteSpecie = async (id) => {
    const config = {
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `JWT ${localStorage.getItem('access')}`,
          'Accept': 'application/json'
      }
  }; 
    await axios.delete(`http://127.0.0.1:8000/species/${id}`, config)
    .then(() => {
      window.location.reload()
    })
  }

  useEffect(() => {
    load_species()
  }, [])


  return (
    <div className="container">
      <h1>specie</h1>
      <Link to='specie/add'>
        <button type="button" class="btn btn-success">Success</button>
      </Link>
      <div className="py-4">
        <h1>Home Page</h1>
        <table class="table border shadow">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {specie.map((XD, index) => (
              <tr>
                <th scope="row">{index + 1}</th>
                <td>{XD.name}</td>
                <td>
                  <Link className="btn btn-primary mr-2" to={`/species/${XD.id}`}>
                    View
                  </Link>
                  <Link
                    className="btn btn-outline-primary mr-2"
                    to={`/species/edit/${XD.id}`}
                  >
                    Edit
                  </Link>
                  <Link
                    className="btn btn-danger"
                    onClick={() => deleteSpecie(XD.id)}
                  >
                    Delete
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  specie: state.specie.specie
})

export default connect(mapStateToProps, { load_species })(Specie);
