import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

const ViewSpecie = () => {
  const { id } = useParams()
  const [spec, setspec] = useState({
    id: "",
    name: "",
    url: ""
  })


  useEffect(() => {
    FetchSpecie()
  }, [])

  const FetchSpecie = async () => {
    const config = {
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `JWT ${localStorage.getItem('access')}`,
          'Accept': 'application/json'
      }
  }; 
    const res = await axios.get(`http://127.0.0.1:8000/species/${id}`, config)
    console.log(res.data)
    setspec(res.data)
  }
//console.log(spec)
  return (
    <div>
      <h3>{spec.id}</h3>
      <h1>{spec.name}</h1>
    </div>
  );
};

export default ViewSpecie;
