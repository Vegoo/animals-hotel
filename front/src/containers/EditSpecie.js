import React, { useEffect, useState } from "react";
import { Redirect, useParams } from "react-router-dom";
import axios from "axios";

const EditSpecie = (props) => {
  
  const { id } = useParams()
  const [ok, setok] = useState(false)
  const [Specie, setSpecie] = useState({
    id: "",
    name: "",
    url: ""
  })

  //const dupa = Specie
  //console.log(dupa)

  useEffect(() => {
    const config = {
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `JWT ${localStorage.getItem('access')}`,
          'Accept': 'application/json'
      }
  }; 
    axios.get(`http://127.0.0.1:8000/species/${id}`, config)
    .then((res) => {
      setSpecie(res.data)
    })
  }, [])
 
  const change = (e) => {
    setSpecie({...Specie, [e.target.name]: e.target.value})
  }

  const submit = async (e) => {
    e.preventDefault();
    const config = {
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `JWT ${localStorage.getItem('access')}`,
          'Accept': 'application/json'
      }
  }; 
    //const body = JSON.stringify(...Specie , {id, name, url})
  const body = {
    "id": Specie.id,
    "name": Specie.name,
    "url": Specie.url
  }
  
    console.log(body)
    axios.put(`http://127.0.0.1:8000/species/${id}`, body, config)
    setok(true)
  }

  if (ok) {
    return <Redirect to='/specie'/>
  }

  return (
    <div className='container'>
        <form>
            <div className="form-group">
                <label >Edit</label>
                <input type="text" className="form-control" name="name" value={Specie.name} onChange={change}></input>
            </div>
            <button type="submit" className="btn btn-primary" onClick={submit}>Register</button>
        </form>
    </div>
  );
};

export default EditSpecie;
