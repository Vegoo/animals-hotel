import React from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import Home from "./containers/Home";
import Animal from "./containers/Animal";
import Reservation from "./containers/Reservation";
import Specie from "./containers/Specie";
import Login from "./containers/Login";
import Register from "./containers/Register";
import AddSpecie from "./containers/AddSpecie";
import Layout from './hocs/Layout'
import EditSpecie from './containers/EditSpecie'
import ViewSpecie from './containers/ViewSpecie'
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import NotFound from "./containers/NotFound";


function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/animal" component={Animal} />
          <Route exact path="/reservation" component={Reservation} />
          <Route exact path="/specie" component={Specie} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/specie/add" component={AddSpecie} />
          <Route exact path="/species/edit/:id" component={EditSpecie} />
          <Route exact path="/species/:id" component={ViewSpecie} />
          <Route component={NotFound} />
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
