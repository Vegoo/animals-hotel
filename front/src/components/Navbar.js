import React, { Fragment, useState } from 'react';
import { Link, Redirect, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../actions/auth';

const Navbar = ({ logout, isAuthenticated }) => {
    const [redirect, setRedirect] = useState(false);

    const logout_user = () => {
        logout();
        setRedirect(true);
    };

    const guestLinks = () => (
      <Fragment>
        <Link className="btn btn-outline-light" to="/register">Register</Link>
        <Link className="btn btn-outline-light" to="/login">Login</Link>
      </Fragment>
    );

    const authLinks = () => (
      <Fragment>
        <Link className="btn btn-outline-light" to="#" onClick={logout_user}>Logout</Link>
      </Fragment>
    );

    return (
      <Fragment>
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        <div className="container">
          <Link className="navbar-brand" to="/">
            Animal Hotel
          
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          </Link>
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <NavLink className="nav-link" exact to="/animal">
                  Animal
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" exact to="/reservation">
                  Reservation
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" exact to="/specie">
                  Specie
                </NavLink>
              </li>
            </ul>
          </div>
          {isAuthenticated ? authLinks() : guestLinks()}
        </div>
      </nav> 
      {redirect ? <Redirect to='/' /> : <Fragment></Fragment>}
    </Fragment>
    );
};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { logout })(Navbar);