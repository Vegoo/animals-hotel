from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin


class Species(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


# class Role(models.Model):
#    name = models.CharField(max_length=50)
#
#    def __str__(self):
#        return self.name


class CustomAccountManager(BaseUserManager):

    def create_user(self, email, name, surname, password=None):
        if not email:
            raise ValueError("You mast provide an email address")
        if not name:
            raise ValueError("You mast provide an name")
        if not surname:
            raise ValueError("You mast provide an surname")

        user = self.model(
            email=self.normalize_email(email),
            name=name,
            surname=surname
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, name, surname, password):
        user = self.create_user(
            email=self.normalize_email(email),
            name=name,
            surname=surname,
            password=password
        )
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=50, null=False)
    surname = models.CharField(max_length=50, null=False)
    password = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    # role = models.ForeignKey(Role, on_delete=models.CASCADE, default=1, null=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = CustomAccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'surname']

    def __str__(self):
        return self.name + " " + self.surname


class Animal(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    species = models.ForeignKey(Species, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Reservation(models.Model):
    startDate = models.DateTimeField()
    endDate = models.DateTimeField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    animal = models.ForeignKey(Animal, on_delete=models.CASCADE)
    comments = models.TextField()
    cost = models.DecimalField(max_digits=6, decimal_places=2)
