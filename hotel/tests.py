from rest_framework.test import APITestCase, APIClient
from rest_framework.reverse import reverse
from . import views
from .models import Species, User
from rest_framework import status
from django.utils.http import urlencode
from django import urls
import json
from rest_framework_simplejwt.tokens import RefreshToken
from django.test import TestCase


class TestCaseBase(TestCase):
    @property
    def bearer_token_for_admin(self):
        user = User.objects.create_superuser('admin@admin.com', 'Admin', 'Adminowski', '123')
        client = APIClient()
        refresh = RefreshToken.for_user(user)
        return {"HTTP_AUTHORIZATION": f'JWT {refresh.access_token}'}

    @property
    def bearer_token_for_user(self):
        user = User.objects.create_user('user@user.com', 'User', 'Userski', '123')
        client = APIClient()
        refresh = RefreshToken.for_user(user)
        return {"HTTP_AUTHORIZATION": f'JWT {refresh.access_token}'}


class SpeciesTest(TestCaseBase):
    url = reverse(views.SpeciesList.name)

    def test_post_and_get_species_with_admin(self):
        response = self.client.post(self.url, data={"name": "dzik"}, **self.bearer_token_for_admin)
        assert response.status_code == status.HTTP_201_CREATED
        assert Species.objects.get().name == "dzik"
        assert Species.objects.count() == 1

    def test_post_species_with_user(self):
        response = self.client.post(self.url, data={"name": "dzik"}, **self.bearer_token_for_user)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_get_species_with_user(self):
        response = self.client.get(self.url, **self.bearer_token_for_user)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_post_species_without_Auth(self):
        response = self.client.post(self.url, data={"name": "zwierz"})
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_get_species_without_Auth(self):
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


class UserTest(TestCaseBase):
    url = reverse(views.UserList.name)
    urlP = reverse(views.ChangePasswordView.name)
    data = {
        "email": "user@user.com",
        "name": "User",
        "surname": "Userski",
        "password": "123"
    }
    dataP = {
        "old_password": "123",
        "new_password": "noweHaslo"
    }

    def test_Register(self):
        response = self.client.post(self.url, data=self.data)
        assert response.status_code == status.HTTP_201_CREATED

    def test_Register_with_auth(self):
        response = self.client.post(self.url, data=self.data, **self.bearer_token_for_user)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_get_user(self):
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK

    def test_change_password_without_auth(self):
        response = self.client.put(self.urlP, data=self.dataP)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_change_password_with_auth(self):
        response = self.client.put(self.urlP, data=self.dataP, content_type='application/json', **self.bearer_token_for_user)
        assert response.status_code == status.HTTP_200_OK

    def test_get_users(self):
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK
