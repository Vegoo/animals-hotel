from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User


class AccountAdmin(UserAdmin):
    list_display = ('email', 'name', 'surname')
    search_fields = ('email', 'name')
    readonly_fields = ('id', 'created')
    ordering = ['name']

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()


admin.site.register(User, AccountAdmin)
