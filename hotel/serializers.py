from rest_framework import serializers
from .models import Species, User, Animal, Reservation


class SpeciesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Species
        fields = ['id', 'url', 'name']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'url', 'email', 'name', 'surname', 'password']

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class ChangePasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['old_password', 'new_password']

    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class AnimalSerializer(serializers.HyperlinkedModelSerializer):
    species = serializers.SlugRelatedField(queryset=Species.objects.all(), slug_field='name')
    owner = serializers.ReadOnlyField(
        source='owner.id')  # serializers.HyperlinkedRelatedField(many=False, queryset=User.objects.all(), view_name='user-detail')

    class Meta:
        model = Animal
        fields = ['id', 'url', 'name', 'age', 'species', 'owner']


class ReservationSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.ReadOnlyField(source='user.id')
    # serializers.HyperlinkedRelatedField(queryset=User.objects.all(), view_name='user-detail')
    animal = serializers.HyperlinkedRelatedField(queryset=Animal.objects.all(), view_name='animal-detail')

    class Meta:
        model = Reservation
        fields = ['id', 'url', 'startDate', 'endDate', 'user', 'animal', 'comments', 'cost']
