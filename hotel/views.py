from rest_framework import generics
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import permissions
from rest_framework import status
from rest_framework.views import APIView
from django_filters import AllValuesFilter, DateTimeFilter, NumberFilter, FilterSet
from .models import Species, User, Animal, Reservation
from .serializers import SpeciesSerializer, UserSerializer, AnimalSerializer, ReservationSerializer, ChangePasswordSerializer
from .custompermission import IsCurrentUserOwner, IsNotAuth, IamOwner, IsAdminUserOrReadOnly
from rest_framework_simplejwt.tokens import RefreshToken
from django.db.models import Prefetch


class SpeciesList(generics.ListCreateAPIView):
    queryset = Species.objects.all()
    serializer_class = SpeciesSerializer
    name = 'species-list'
    permission_classes = [permissions.IsAdminUser]
    filterset_fields = ['name']
    search_fields = ['name']
    ordering_fields = ['name']


class SpeciesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Species.objects.all()
    serializer_class = SpeciesSerializer
    name = 'species-detail'
    permission_classes = [permissions.IsAdminUser]


class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    name = 'user-list'
    permission_classes = [IsNotAuth]
    filterset_fields = ['name', 'surname', 'email']
    search_fields = ['surname', 'email']
    ordering_fields = ['name', 'surname', 'email']


class ChangePasswordView(generics.UpdateAPIView):
    serializer_class = ChangePasswordSerializer
    model = User
    name = 'change-password'
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    name = 'user-detail'
    permission_classes = [permissions.IsAuthenticated, IamOwner]


class AnimalList(generics.ListCreateAPIView):
    serializer_class = AnimalSerializer
    name = 'animal-list'
    permission_classes = [permissions.IsAuthenticated]
    filterset_fields = ['name', 'age']
    search_fields = ['name']
    ordering_fields = ['name', 'age']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        user = self.request.user.id
        queryset = Animal.objects.filter(owner=user)
        return queryset


class AnimalDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer
    name = 'animal-detail'
    permission_classes = [IsCurrentUserOwner]


class ReservationList(generics.ListCreateAPIView):
    queryset = Reservation.objects.prefetch_related(Prefetch('animal', queryset=Animal.objects.filter(owner=9)))
    serializer_class = ReservationSerializer
    permission_classes = [permissions.IsAuthenticated]
    name = 'reservation-list'

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ReservationDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
    name = 'reservation-detail'


class BlackListTokenView(APIView):
    def post(self, request):
        try:
            refresh_token = request.data["refresh-token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ApiRoot(generics.GenericAPIView):
    name = "api-root"

    def get(self, request, *args, **kwargs):
        return Response({
            'species': reverse(SpeciesList.name, request=request),
            'user': reverse(UserList.name, request=request),
            'animals': reverse(AnimalList.name, request=request),
            'reservations': reverse(ReservationList.name, request=request)
        })
